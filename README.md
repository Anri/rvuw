# Razer Viper Ultimate on Windows

> Without insanely bad Razer software.

## Roadmap

- [ ] Find the percentage of the battery
- [ ] Find the status of the battery

## Features

- [ ] Run in background
- [ ] Icon in Taskbar
  - [ ] Labels who show status and percentage
  - [ ] Checkbox for running at boot
  - [ ] Button to close the app

## Acknowledgment

Thanks [openrazer](https://github.com/openrazer/openrazer/) for all the knowledge.

---

## Developer documentation

### Build

I'm using WSL + MinGW, running `make` should work.

### Development

I'm using VSCode with clangd. It should work out of the box,
see [.vscode](./.vscode) directory.
