#include "../includes/battery.h"

#include <windows.h>

#include <hidsdi.h>
#include <stdio.h>

#define RAZER_REPORT_LEN 0x5A
#define RAZER_REPORT_ID_BATTERY 0x03
#define RAZER_BATTERY_LEVEL_INDEX 3
#define RAZER_CHARGING_STATUS_INDEX 4

/*****************************************************************************
 *
 *                       This code doesn't work.
 *
 *************************************************************************** */

int get_battery_level(const char *device_path, bool *is_charging) {
  HANDLE hDevice;
  if ((hDevice = CreateFile(device_path, GENERIC_READ | GENERIC_WRITE,
                            FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                            OPEN_EXISTING, 0, NULL)) == INVALID_HANDLE_VALUE) {
    fprintf_s(stderr, "[%lu] Failed to open device.\n", GetLastError());

    return -1;
  }

  unsigned char report[RAZER_REPORT_LEN] = {0};

  // Prepare the report to request battery information
  report[0] = RAZER_REPORT_ID_BATTERY;

  // Send the report
  if (!HidD_SetFeature(hDevice, report, RAZER_REPORT_LEN)) {
    fprintf_s(stderr, "[%lu] Failed to send report to device.\n",
              GetLastError());

    CloseHandle(hDevice);
    return -1;
  }

  // Read the response
  if (!HidD_GetFeature(hDevice, report, RAZER_REPORT_LEN)) {
    fprintf_s(stderr, "[%lu] Failed to read report from device.\n",
              GetLastError());

    CloseHandle(hDevice);
    return -1;
  }

  // Extract battery level
  int batteryLevel = report[RAZER_BATTERY_LEVEL_INDEX];

  // Extract charging status
  *is_charging = (report[RAZER_CHARGING_STATUS_INDEX] != 0);

  CloseHandle(hDevice);
  return batteryLevel;
}
