#include "../includes/battery.h"
#include "../includes/helper.h"

#include <stdio.h>
#include <string.h>

int main(void) {
  char device_path[2048];
  unsigned long long min_len = 10;

  // Try wired
  find_device_path(USB_DEVICE_ID_RAZER_VIPER_ULTIMATE_WIRED, device_path);

  if (strlen(device_path) < min_len) {
    // Try wireless
    find_device_path(USB_DEVICE_ID_RAZER_VIPER_ULTIMATE_WIRELESS, device_path);

    if (strlen(device_path) < min_len) {
      fprintf_s(stderr, "Can't find the Razer Viper Ultimate mouse.\n");

      return 1;
    }
  }
  printf("Mouse found: %s\n", device_path);

  bool is_charging;
  int battery_level;
  if ((battery_level = get_battery_level(device_path, &is_charging)) < 0) {
    fprintf_s(stderr, "Failed to get battery level.\n");

    return 1;
  }

  printf("Battery Level: %d%%\n", battery_level);
  printf("Charging: %s\n", is_charging ? "Yes" : "No");

  return 0;
}
