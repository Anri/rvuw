#include "../includes/helper.h"

#include <windows.h>

#include <hidsdi.h>
#include <setupapi.h>
#include <stdio.h>

#define VID_RAZER 0x1532

void find_device_path(int pid, char *device_path) {
  GUID hidGuid;
  HidD_GetHidGuid(&hidGuid);

  HDEVINFO hDevInfo = SetupDiGetClassDevs(
      &hidGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
  if (hDevInfo == INVALID_HANDLE_VALUE) {
    fprintf_s(stderr, "[%lu] Failed to get any device information\n",
              GetLastError());

    return;
  }

  SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
  deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

  for (DWORD i = 0; SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &hidGuid, i,
                                                &deviceInterfaceData);
       ++i) {
    DWORD requiredSize;
    SetupDiGetDeviceInterfaceDetail(hDevInfo, &deviceInterfaceData, NULL, 0,
                                    &requiredSize, NULL);

    PSP_DEVICE_INTERFACE_DETAIL_DATA detailData;
    if (!(detailData =
              (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(requiredSize))) {
      fprintf_s(
          stderr,
          "Failed to allocated memory for PSP_DEVICE_INTERFACE_DETAIL_DATA.\n");

      return;
    }
    detailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

    if (SetupDiGetDeviceInterfaceDetail(hDevInfo, &deviceInterfaceData,
                                        detailData, requiredSize, NULL, NULL)) {
      HANDLE hDevice = CreateFile(
          detailData->DevicePath, GENERIC_READ | GENERIC_WRITE,
          FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

      if (hDevice != INVALID_HANDLE_VALUE) {
        // Get device attributes
        HIDD_ATTRIBUTES attributes;
        attributes.Size = sizeof(HIDD_ATTRIBUTES);
        if (HidD_GetAttributes(hDevice, &attributes)) {
          // Check VID and PID
          if (attributes.VendorID == VID_RAZER && attributes.ProductID == pid) {
            strcpy(device_path, detailData->DevicePath);

            CloseHandle(hDevice);
            break;
          }
        }

        CloseHandle(hDevice);
      }
    }

    free(detailData);
  }

  SetupDiDestroyDeviceInfoList(hDevInfo);
}
