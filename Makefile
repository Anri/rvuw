CC = x86_64-w64-mingw32-gcc
RM = rm

SOURCES = $(wildcard src/*.c)
OBJECTS = $(patsubst %.c,%.o,$(notdir $(SOURCES)))

CFLAGS  = -std=gnu17 -pedantic
LDFLAGS = -lhid -lsetupapi

EXE     = rvuw
EXE_EXT = exe

%.o: src/%.c
	$(CC) -c $< -o $@ $(CFLAGS)

release: CFLAGS += -O3
release: compilation

debug: CFLAGS += -Wall -Wextra -Wshadow -Wcast-align -fanalyzer -g -Og
debug: compilation

compilation: $(OBJECTS)
	$(CC) -o $(EXE).$(EXE_EXT) $(OBJECTS) $(LDFLAGS)

all:
	release

clean:
	$(RM) $(OBJECTS) $(EXE).$(EXE_EXT)
