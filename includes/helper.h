#pragma once

#define USB_DEVICE_ID_RAZER_VIPER_ULTIMATE_WIRED 0x007A
#define USB_DEVICE_ID_RAZER_VIPER_ULTIMATE_WIRELESS 0x007B

// Enumerate through HID devices and find device path
void find_device_path(int pid, char *device_path);
