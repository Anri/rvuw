#pragma once

#include <stdbool.h>

// Get the current battery level
int get_battery_level(const char *device_path, bool *is_charging);
